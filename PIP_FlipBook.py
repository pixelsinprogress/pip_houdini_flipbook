import hou, os
import toolutils
from datetime import datetime

def ExportScreenshot():

	#Get the current Desktop
	desktop = hou.ui.curDesktop()

	# Get the scene viewer
	scene_view = toolutils.sceneViewer()

	#Create the flipbook settings data 
	now		  = datetime.now()
	dt_string = now.strftime("%y%m%d__%H_%M_%S")

	#Make the directiry if it does not exist
	directory =  hou.expandString("$HIP/screenshots")
	if os.path.exists(directory) == False: os.mkdir(directory)


	fn = directory + "/" + dt_string + ".jpg"

	# Copy the viewer's current flipbook settings
	flipbook_options = scene_view.flipbookSettings().stash()

	#Current framne
	cf = int(hou.expandString("$F"))


	flipbook_options.output(fn)
	flipbook_options.frameRange( (cf, cf) )
	flipbook_options.beautyPassOnly(False)
	flipbook_options.visibleTypes(hou.flipbookObjectType.GeoOnly)
	flipbook_options.outputToMPlay(False)

	cam = scene_view.curViewport().camera()

	if cam:
		rx = cam.parm("resx").eval()
		ry = cam.parm("resy").eval()

		flipbook_options.useResolution = True
		flipbook_options.resolution( (rx, ry) )
	

	scene_view.flipbook(viewport=None, settings=flipbook_options, open_dialog=False)


	return
